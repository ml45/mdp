import numpy as np
import pandas as pd

import gym
import itertools
import time
import sys
import pickle

import matplotlib.pyplot as plt


class BaseAgent(object):
    def __init__(self, env_name, **kwargs):
        self.num_iters = 0
        self.Pi = None
        self.run_stats = []

    def solve(self):
        pass

    def policy_at_state(self, s):
        pass


# https://marcinbogdanski.github.io/rl-sketchpad/RL_An_Introduction_2018/0403_Policy_Iteration.html
class PolicyIteration(BaseAgent):
    def __init__(self, env_name, theta=0.001, gamma=0.9, **kwargs):
        super().__init__(env_name, **kwargs)
        self.env = gym.make(env_name, **kwargs)
        self.episode_rewards = []
        self.V = np.zeros(shape=self.env.nS)
        self.Pi = np.random.randint(self.env.nA, size=self.env.nS)

        self.theta = theta
        self.gamma = gamma
        self.num_eval_iter = 0
        self.num_impr_iter = 0
        self.run_stats = []
        self.start_time = 0

    def solve(self):
        self.start_time = time.time()

        def sum_sr(env, V, s, a, gamma):
            """Calc state-action value for state 's' and action 'a'"""
            tmp = 0  # state value for state s
            for p, s_, r, _ in env.P[s][a]:  # see note #1 !
                tmp += p * (r + gamma * V[s_])
            return tmp

        def policy_evaluation():
            while True:
                delta = 0
                for s in range(self.env.nS):
                    v = self.V[s]
                    self.V[s] = sum_sr(self.env, V=self.V, s=s, a=self.Pi[s], gamma=self.gamma)

                    delta = max(delta, np.abs(v - self.V[s]))
                self.num_eval_iter += 1
                if delta < self.theta:
                    break

        def policy_improvement():
            policy_stable = True
            for s in range(self.env.nS):
                old_action = self.Pi[s]

                self.Pi[s] = np.argmax([sum_sr(self.env, V=self.V, s=s, a=a, gamma=self.gamma)  # list comprehension
                                        for a in range(self.env.nA)])

                if old_action != self.Pi[s]:
                    policy_stable = False
            if policy_stable:
                pass
            return policy_stable

        policy_stable = False
        while not policy_stable:
            iter_start_time = time.time()
            old_V = self.V.copy()
            policy_evaluation()
            error = np.absolute(old_V - self.V).max()

            max_V = np.max(self.V)
            mean_V = np.mean(self.V)

            policy_stable = policy_improvement()
            iter_end_time = time.time()
            iter_time = iter_end_time - iter_start_time
            cum_time = iter_end_time - self.start_time
            self.num_impr_iter += 1
            print(f"{self.num_impr_iter} improvement iters")
            self.run_stats.append({"iter_time":iter_time, 'cum_time':cum_time, "error":error, "mean_V":mean_V, "max_V":max_V})


        print(f"solved in {self.num_impr_iter} policy improvement iters and {self.num_eval_iter} policy eval iters!")

    def policy_at_state(self, s):
        return self.Pi[s]


def test_optimal_simpletetris(agent, verbose=True):
    # exhaustively check if all states e.g. for n=3
    # X_X
    # X_X
    # X_X
    # check if the policy is always to go into the empty column

    n = agent.env.n
    num_wrong = 0

    x = list(range(1, n))
    results = [[p for p in itertools.product(x, repeat=n)] for _ in range(n)]
    final_results = set()

    for empty_i in range(n):
        for item_i in range(len(results[empty_i])):
            item = list(results[empty_i][item_i])
            item[empty_i] = 0
            results[empty_i][item_i] = tuple(item)
        final_results.update(results[empty_i])

    num_tested = 0
    for test in final_results:
        empty_i = np.argmin(test)
        s = agent.env.convert_heights_to_coded_state(test)
        num_tested += 1
        if agent.Pi[s] != empty_i:
            num_wrong += 1


    if verbose:
        print(f"got {num_wrong} wrong out of {num_tested} tested")
    return (num_wrong, num_tested)


if __name__ == '__main__':
    np.random.seed(1337)

    #env_name = 'gym_simpletetris:simpletetris-v0'
    # env_name = "FrozenLake8x8-v0"
    # env_name = "FrozenLake-v0"

    #
    # env_name ='gym_simpletetris:simpletetris-v0'
    # n = 6
    # alpha = 1.0
    # gamma = 0.9
    # epsilon_start = 1.0
    # epsilon_decay = 0.9999
    # episodes = 100000

    # q.py env_name n alpha gamma epsilon_start epsilon_decay episodes
    # pi.py env_name n theta gamma
    env_name = sys.argv[1]
    short_env_name = env_name.split(":")[0]
    n = int(sys.argv[2])
    theta = float(sys.argv[3])
    gamma = float(sys.argv[4])

    #n = 6
    if env_name.startswith("Frozen"):
        learner = PolicyIteration(env_name, gamma=gamma, theta=theta)
    else:
        learner = PolicyIteration(env_name, n=n, gamma=gamma, theta=theta)

    #learner = PolicyIteration(env_name, n=n, gamma=0.9, theta=1e-8)
    #learner = ValueIteration(env_name, n=n, gamma=0.5, theta=1e-8)
    #learner = QLearningAgent(env_name, n=n, alpha=alpha,gamma=gamma, epsilon_start=epsilon_start, epsilon_decay=epsilon_decay, episodes=episodes)


    out_folder = f"results/{short_env_name}/pi"
    out_file_prefix = f"pi_n={n}_theta={theta}_gamma={gamma}"

    learner.solve()
    if env_name.startswith("Frozen"):
        pd.DataFrame(learner.Pi).to_csv(f"{out_folder}/{out_file_prefix}_policy.csv", index=False)
    else:
        num_wrong, num_tested = test_optimal_simpletetris(learner)
        pd.DataFrame(learner.Pi).to_csv(f"{out_folder}/{out_file_prefix}_policy_{num_wrong}wrong-out-of-{num_tested}.csv", index=False)

    run_stats_df = pd.DataFrame(learner.run_stats)
    run_stats_df.to_csv(f"{out_folder}/{out_file_prefix}_runstats.csv")

    pickle.dump(learner, open(f"{out_folder}/{out_file_prefix}_learner.p", "wb"))


    # moves = {0: '←', 1: '↓', 2: '→', 3: '↑'}
    # print(np.reshape([moves[a] for a in learner.Pi], (n, n)))

    # pass
    # ers = pd.DataFrame(learner.episode_rewards)
    # ers.to_csv("ers.csv")
