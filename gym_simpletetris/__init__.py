from gym.envs.registration import register

register(
    id='simpletetris-v0',
    entry_point='gym_simpletetris.envs:SimpleTetrisEnv',
)
register(
    id='simpletetris-extrahard-v0',
    entry_point='gym_simpletetris.envs:SimpleTetrisExtraHardEnv',
)