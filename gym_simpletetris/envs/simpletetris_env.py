import gym
from gym import error, spaces, utils, Env
from gym.utils import seeding

from gym.envs.toy_text import discrete

import numpy as np
import os
import pickle


class SimpleTetrisEnv(discrete.DiscreteEnv):
    metadata = {'render.modes': ['human']}

    def __init__(self, n=6, skip_P=False):
        self.n = n
        self.board = np.zeros(shape=(self.n, self.n))

        self.nS = self.n ** self.n
        self.nA = self.n
        self.P = {s: {a: [] for a in range(self.nA)} for s in range(self.nS)}
        p_fname = f"simpletetris_n={self.n}_P.p"

        if os.path.exists(p_fname):
            self.P = pickle.load(open(p_fname, "rb"))
        else:
            if not skip_P:
                for s in range(self.nS):
                    for a in range(self.nA):
                        next_states = []
                        self.cum_reward = 0

                        # a could be leftmost column, a center column, or the rightmost column

                        # leftmost. 80% of doing a, 20% of doing a+1
                        if a == 0:
                            self.set_state(s)
                            next_state, reward, loss, _ = self.force_step(a)
                            next_states.append((0.8, next_state, reward, loss))

                            self.set_state(s)
                            next_state, reward, loss, _ = self.force_step(a+1)
                            next_states.append((0.2, next_state, reward, loss))

                        elif a == self.n - 1:  # rightmost. 80% of doing a, 20% chance of doing a-1
                            self.set_state(s)
                            next_state, reward, loss, _ = self.force_step(a)
                            next_states.append((0.8, next_state, reward, loss))

                            self.set_state(s)
                            next_state, reward, loss, _ = self.force_step(a - 1)
                            next_states.append((0.2, next_state, reward, loss))
                        else:  # center. 60% of a, 20% of a-1, 20% of a+1
                            self.set_state(s)
                            next_state, reward, loss, _ = self.force_step(a)
                            next_states.append((0.6, next_state, reward, loss))

                            self.set_state(s)
                            next_state, reward, loss, _ = self.force_step(a - 1)
                            next_states.append((0.2, next_state, reward, loss))

                            self.set_state(s)
                            next_state, reward, loss, _ = self.force_step(a + 1)
                            next_states.append((0.2, next_state, reward, loss))

                        self.P[s][a] = next_states
                self.set_state(0)
                pickle.dump(self.P, open(p_fname, "wb"))

        isd = np.zeros(self.nS)
        isd[0] = 1.0

        self.cum_reward = 0
        super(SimpleTetrisEnv, self).__init__(self.nS, self.nA, self.P, isd)

    def set_state(self, encoded_board):
        decoded_heights = []
        for col in range(self.n):
            remainder = encoded_board % self.n
            encoded_board -= remainder
            encoded_board /= self.n
            decoded_heights.append(int(remainder))
        decoded_heights = decoded_heights[::-1]

        new_board_T = np.zeros(shape=(self.n, self.n))

        col_i = 0
        for height in decoded_heights:
            zeros = self.n - height
            temp_col = [0.0 for _ in range(zeros)]
            for _ in range(height):
                temp_col.append(1.0)
            new_board_T[col_i] = temp_col
            col_i += 1
        new_board = new_board_T.T
        self.board = new_board
        pass

    def get_state(self):
        board_T = self.board.T
        s_out = 0

        for col_i in range(self.n):
            s_out += np.count_nonzero(board_T[col_i])
            if col_i != self.n - 1:
                s_out *= self.n

        return s_out

    def convert_heights_to_coded_state(self, heights):
        s_out = 0
        for col_i in range(self.n):
            s_out += heights[col_i]
            if col_i != self.n - 1:
                s_out *= self.n

        return s_out

    def get_next_states(self):
        outputs = []
        for a in range(self.nA):
            temp_env = SimpleTetrisEnv(n=self.n, skip_P=True)
            temp_env.board = self.board.copy()
            next_state, reward, loss, _ = temp_env.step(a)

            outputs.append({'p': 1, 'sprime': next_state, 'r': reward, 'done': loss})
        return outputs

    def force_step(self, action):
        reward = 0
        loss = False

        board_T = self.board.T
        col = board_T[action]
        if np.count_nonzero(col) == 0:
            first_open_colpos = self.n - 1
        else:
            first_open_colpos = np.min(np.where(col != 0)) - 1

        if first_open_colpos < 1:
            # loss
            reward = -self.n
            loss = True
        else:
            col[first_open_colpos] = 1
            # reward = 1

        # check bottom row for completion
        bottom_row = self.board[self.n - 1]
        if np.count_nonzero(bottom_row) == self.n:
            self.board = np.roll(self.board, 1, 0)
            self.board[0] = np.zeros(self.n)
            reward = self.n

        self.cum_reward += reward
        # don't let it go on forever...
        if self.cum_reward > 20*self.n:
            loss = True

        return self.get_state(), reward, loss, {}

    def step(self, action):
        # take action with 60% of the time, or 20% go one left or right instead
        # if on a side edge of the screen, that 20% that'd be off screen goes back to action
        acceptable_actions = [action]
        if action != 0:
            acceptable_actions.append(action - 1)
        if action != self.n - 1:
            acceptable_actions.append(action + 1)

        if len(acceptable_actions) == 2:
            choice = np.random.choice(acceptable_actions, p = [0.8, 0.2])
        else:
            choice = np.random.choice(acceptable_actions, p = [0.6, 0.2, 0.2])

        return self.force_step(choice)

    def reset(self):
        self.board = np.zeros(shape=(self.n, self.n))
        self.cum_reward = 0
        return self.get_state()

    def render(self, mode='human'):
        for row in self.board:
            for col in row:
                print("█" if col == 1.0 else "░", end='')
            print()
        pass

    def close(self):
        pass
