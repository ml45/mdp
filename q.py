import numpy as np
import pandas as pd

import gym
import itertools
import time
import sys
import pickle

import matplotlib.pyplot as plt


class BaseAgent(object):
    def __init__(self, env_name, **kwargs):
        self.num_iters = 0
        self.Pi = None
        self.run_stats = []

    def solve(self):
        pass

    def policy_at_state(self, s):
        pass


class QLearningAgent(BaseAgent):
    def __init__(self, env_name, alpha=0.9, gamma=0.9, epsilon_start=1.0, epsilon_decay=0.9999, episodes=50000, **kwargs):
        super().__init__(env_name, **kwargs)
        self.Q = None
        self.V = None
        self.alpha = alpha
        self.gamma = gamma
        self.epsilon = epsilon_start
        self.epsilon_decay = epsilon_decay
        self.env = gym.make(env_name, **kwargs)
        self.num_episodes = episodes

        self.Pi = np.zeros(shape=self.env.nS, dtype=int)

        self.episode_rewards = []

        #n = self.env.n
        x = list(range(1, n))
        results = [[p for p in itertools.product(x, repeat=n)] for _ in range(n)]
        self.optimal_tests = set()

        for empty_i in range(n):
            for item_i in range(len(results[empty_i])):
                item = list(results[empty_i][item_i])
                item[empty_i] = 0
                results[empty_i][item_i] = tuple(item)
            self.optimal_tests.update(results[empty_i])

    def tetris_test(self):
        num_wrong = 0
        num_tested = 0
        for test in self.optimal_tests:
            empty_i = np.argmin(test)
            s = self.env.convert_heights_to_coded_state(test)
            num_tested += 1
            if self.Pi[s] != empty_i:
                num_wrong += 1
        return (num_wrong, num_tested)

    def solve(self):
        self.Q = np.zeros(shape=(self.env.nS, self.env.nA))

        def choose_a(_s, epsilon):
            if np.random.random() < epsilon:
                return np.random.randint(0, self.env.nA)
            else:
                return np.argmax(self.Q[_s])

        q_start_time = time.time()
        num_episodes = self.num_episodes
        cum_steps = 0
        for episode_i in range(num_episodes):
            episode_start_time = time.time()
            s = self.env.reset()
            d = False
            episode_reward = 0
            episode_steps = 0
            while not d:
                a = choose_a(s, self.epsilon)
                sprime, r, d, _ = self.env.step(a)
                episode_reward += r
                self.Q[s, a] += self.alpha * (r + self.gamma * np.max(self.Q[sprime]) - self.Q[s, a])
                s = sprime
                episode_steps += 1
                cum_steps += 1
            self.epsilon *= self.epsilon_decay
            self.episode_rewards.append(episode_reward)


            self.Pi = np.argmax(self.Q, axis=1)

            #num_wrong, num_tested = test_optimal_simpletetris(self, verbose=False)
            #num_wrong, num_tested = self.tetris_test()
            num_wrong, num_tested = (0,0)
            self.V = np.max(self.Q, axis=1)
            max_V = np.max(self.V)
            mean_V = np.mean(self.V)
            episode_end_time = time.time()
            episode_time = episode_end_time - episode_start_time
            cum_time = episode_end_time - q_start_time

            self.run_stats.append({
                'episode_i':episode_i, 'epsilon':self.epsilon, 'episode_reward':episode_reward,
                'max_V':max_V, 'mean_V':mean_V, "episode_steps":episode_steps, 'cum_steps':cum_steps,
                'episode_time':episode_time, 'cum_time':cum_time,
                 })
            if episode_i % (num_episodes // 10) == 0:
                print(f"done episode #{episode_i}/{num_episodes}!")

        for s in range(self.env.nS):
            self.Pi[s] = np.argmax(self.Q[s])

    def policy_at_state(self, s):
        return np.argmax(self.Q[s])


def test_optimal_simpletetris(agent, verbose=True):
    # exhaustively check if all states e.g. for n=3
    # X_X
    # X_X
    # X_X
    # check if the policy is always to go into the empty column

    n = agent.env.n
    num_wrong = 0

    x = list(range(1, n))
    results = [[p for p in itertools.product(x, repeat=n)] for _ in range(n)]
    final_results = set()

    for empty_i in range(n):
        for item_i in range(len(results[empty_i])):
            item = list(results[empty_i][item_i])
            item[empty_i] = 0
            results[empty_i][item_i] = tuple(item)
        final_results.update(results[empty_i])

    num_tested = 0
    for test in final_results:
        empty_i = np.argmin(test)
        s = agent.env.convert_heights_to_coded_state(test)
        num_tested += 1
        if agent.Pi[s] != empty_i:
            num_wrong += 1


    if verbose:
        print(f"got {num_wrong} wrong out of {num_tested} tested")
    return (num_wrong, num_tested)


if __name__ == '__main__':
    np.random.seed(1337)


    # q.py env_name n alpha gamma epsilon_start epsilon_decay episodes
    env_name = sys.argv[1]
    short_env_name = env_name.split(":")[0]
    n = int(sys.argv[2])
    alpha = float(sys.argv[3])
    gamma = float(sys.argv[4])
    epsilon_start = float(sys.argv[5])
    epsilon_decay = float(sys.argv[6])
    episodes = int(sys.argv[7])

    if env_name.startswith("Frozen"):
        learner = QLearningAgent(env_name, alpha=alpha,gamma=gamma, epsilon_start=epsilon_start, epsilon_decay=epsilon_decay, episodes=episodes)
    else:
        learner = QLearningAgent(env_name, n=n, alpha=alpha,gamma=gamma, epsilon_start=epsilon_start, epsilon_decay=epsilon_decay, episodes=episodes)


    out_folder = f"results/{short_env_name}/q"
    out_file_prefix = f"q_n={n}_alpha={alpha}_gamma={gamma}_ep-start={epsilon_start}_ep-decay={epsilon_decay}_episodes={episodes}"

    learner.solve()
    if env_name.startswith("Frozen"):
        pd.DataFrame(learner.Pi).to_csv(f"{out_folder}/{out_file_prefix}_policy.csv", index=False)
    else:
        num_wrong, num_tested = test_optimal_simpletetris(learner)
        pd.DataFrame(learner.Pi).to_csv(f"{out_folder}/{out_file_prefix}_policy_{num_wrong}wrong-out-of-{num_tested}.csv", index=False)

    run_stats_df = pd.DataFrame(learner.run_stats)
    run_stats_df.to_csv(f"{out_folder}/{out_file_prefix}_runstats.csv")

    pickle.dump(learner, open(f"{out_folder}/{out_file_prefix}_learner.p", "wb"))
