import numpy as np
import pandas as pd

import gym
import itertools
import time
import sys
import pickle

import matplotlib.pyplot as plt


class BaseAgent(object):
    def __init__(self, env_name, **kwargs):
        self.num_iters = 0
        self.Pi = None
        self.run_stats = []

    def solve(self):
        pass

    def policy_at_state(self, s):
        pass


# https://marcinbogdanski.github.io/rl-sketchpad/RL_An_Introduction_2018/0404_Value_Iteration.html
class ValueIteration(BaseAgent):
    def __init__(self, env_name, theta=0.001, gamma=0.1, **kwargs):
        super().__init__(env_name, **kwargs)
        self.env = gym.make(env_name, **kwargs)
        self.episode_rewards = []
        self.V = np.zeros(shape=self.env.nS)
        self.Pi = np.zeros(shape=self.env.nS, dtype=int)

        self.theta = theta
        self.gamma = gamma

        self.num_iters = 0
        self.run_stats = []

    def sum_sr(self, env, V, s, a, gamma):
        """Calc state-action value for state 's' and action 'a'"""
        tmp = 0  # state value for state s
        for p, s_, r, _ in env.P[s][a]:  # see note #1 !
            tmp += p * (r + gamma * V[s_])
        return tmp

    def solve(self):
        start_time = time.time()
        while True:
            delta = 0.0
            old_V = self.V.copy()
            iter_start_time = time.time()
            for s in range(self.env.nS):
                v = self.V[s]
                self.V[s] = max([self.sum_sr(self.env, V=self.V, s=s, a=a, gamma=self.gamma)  # list comprehension
                                 for a in range(self.env.nA)])
                delta = max(delta, np.abs(v - self.V[s]))

            iter_end_time = time.time()
            iter_time = iter_end_time - iter_start_time
            cum_time = iter_end_time - start_time
            error = np.absolute(old_V - self.V).max()
            max_V = np.max(self.V)
            mean_V = np.mean(self.V)
            self.num_iters += 1
            self.run_stats.append({"iter_time":iter_time, 'cum_time':cum_time, "error":error, "mean_V":mean_V, "max_V":max_V})
            if delta < self.theta:
                break

        for s in range(self.env.nS):
            self.Pi[s] = np.argmax([self.sum_sr(self.env, V=self.V, s=s, a=a, gamma=self.gamma)  # list comprehension
                                    for a in range(self.env.nA)])

        print(f"solved in {self.num_iters} iterations!")

    def policy_at_state(self, s):
        self.env.set_state(s)
        next_states = self.env.get_next_states()
        sums = [next_states[a]['p'] * (next_states[a]['r'] + self.gamma * self.V[next_states[a]['sprime']]) for a in
                range(len(next_states))]
        return np.argmax(sums)


def test_optimal_simpletetris(agent, verbose=True):
    # exhaustively check if all states e.g. for n=3
    # X_X
    # X_X
    # X_X
    # check if the policy is always to go into the empty column

    n = agent.env.n
    num_wrong = 0

    x = list(range(1, n))
    results = [[p for p in itertools.product(x, repeat=n)] for _ in range(n)]
    final_results = set()

    for empty_i in range(n):
        for item_i in range(len(results[empty_i])):
            item = list(results[empty_i][item_i])
            item[empty_i] = 0
            results[empty_i][item_i] = tuple(item)
        final_results.update(results[empty_i])

    num_tested = 0
    for test in final_results:
        empty_i = np.argmin(test)
        s = agent.env.convert_heights_to_coded_state(test)
        num_tested += 1
        if agent.Pi[s] != empty_i:
            num_wrong += 1


    if verbose:
        print(f"got {num_wrong} wrong out of {num_tested} tested")
    return (num_wrong, num_tested)


if __name__ == '__main__':
    np.random.seed(1337)

    #env_name = 'gym_simpletetris:simpletetris-v0'
    # env_name = "FrozenLake8x8-v0"
    # env_name = "FrozenLake-v0"

    # vi.py env_name n theta gamma
    env_name = sys.argv[1]
    short_env_name = env_name.split(":")[0]
    n = int(sys.argv[2])
    theta = float(sys.argv[3])
    gamma = float(sys.argv[4])

    if env_name.startswith("Frozen"):
        learner = ValueIteration(env_name, gamma=gamma, theta=theta)
    else:
        learner = ValueIteration(env_name, n=n, gamma=gamma, theta=theta)

    out_folder = f"results/{short_env_name}/vi"
    out_file_prefix = f"vi_n={n}_theta={theta}_gamma={gamma}"

    learner.solve()
    run_stats_df = pd.DataFrame(learner.run_stats)
    run_stats_df.to_csv(f"{out_folder}/{out_file_prefix}_runstats.csv")
    if env_name.startswith("Frozen"):
        pd.DataFrame(learner.Pi).to_csv(f"{out_folder}/{out_file_prefix}_policy.csv", index=False)
    else:
        num_wrong, num_tested = test_optimal_simpletetris(learner)
        pd.DataFrame(learner.Pi).to_csv(f"{out_folder}/{out_file_prefix}_policy_{num_wrong}wrong-out-of-{num_tested}.csv", index=False)

    pickle.dump(learner, open(f"{out_folder}/{out_file_prefix}_learner.p", "wb"))


    # moves = {0: '←', 1: '↓', 2: '→', 3: '↑'}
    # print(np.reshape([moves[a] for a in learner.Pi], (n, n)))

    # pass
    # ers = pd.DataFrame(learner.episode_rewards)
    # ers.to_csv("ers.csv")
