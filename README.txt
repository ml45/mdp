# Assignment 4 - Markov Decision Processes

## About
Timothy Isonio

tisonio3@gatech.edu

CS 7641 - Machine Learning - Spring 2021
Code and extras located at:
https://gitlab.com/ml45/mdp

This is a Markdown document with a .txt extension.

## Environment
In the code repo is the environment.yml/requirements.txt needed to replicate the Conda environment I used
for this project. If that does not work, in essence creating an environment with these:

    python>=3.8 numpy scipy scikit-learn>=0.24 matplotlib pandas jupyter yellowbrick seaborn gym

should work too. I did this project on a Windows 10 PC running an AMD 8-core/16-thread processor,
so there might be some Windows specific stuff in there. Sometimes I hard-coded 14 jobs
so that I had a core to actually use myself while it ran.

See https://github.com/openai/gym/blob/master/docs/creating-environments.md for instructions on how to install
a new OpenAI Gym environment.

## Running the code
Everything is split into its own Python files: q.py, pi.py, and vi.py in this directory. They're all named what they do.

They all take command line arguments and output results to the results folder (which must be present, I don't
actually check if it exists/create it if not). They output run stats, a pickled learner class, and sometimes the
policy.

There is also the folder containing the OpenAI Gym environment I created, SimpleTetris.
See https://github.com/openai/gym/blob/master/docs/creating-environments.md for how to install and use it.

## References

* some of the code was copied from my own homework assignments in the RL class.
* Sutton & Barto
* MDPToolbox-Hiive
* OpenAI Gym
* RL Sketchpad https://marcinbogdanski.github.io/rl-sketchpad/
* The articles also cited in the report