import sys
import numpy as np
import pandas as pd

n = sys.argv[1]
policy_fname = sys.argv[2]
policy = pd.read_csv(policy_fname, index_col=None, header=0)

moves = {0:'←', 1:'↓', 2:'→', 3:'↑'}
print(np.reshape([moves[a] for a in policy['0']], (n,n)))


